using System;

namespace FijoCore.Infrastructure.DbC
{
    public class PostConditionAttribute : Attribute
    {
        public PostConditionAttribute(bool myCondition)
        {
            if (!myCondition) throw new PostConditionException();
        }
    }
}