using System;

namespace FijoCore.Infrastructure.DbC
{
    public class PreConditionException : Exception
    {
        public PreConditionException() { }

        public PreConditionException(string message) : base(message) { }
    }
}