using System;

namespace FijoCore.Infrastructure.DbC
{
    public class PreConditionAttribute : Attribute
    {
        public PreConditionAttribute(bool myCondition)
        {
            if (!myCondition) throw new PreConditionException();
        }
    }
}