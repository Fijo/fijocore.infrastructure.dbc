using System;

namespace FijoCore.Infrastructure.DbC
{
    public class PostConditionException : Exception
    {
        public PostConditionException() { }

        public PostConditionException(string message) : base(message) { }
    }
}